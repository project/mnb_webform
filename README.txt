Requirements
------------

This module currently requires Drupal 7.x, Webform 3.x, the Libraries API
module, and a copy of the MNB_PHP_API library in your libraries folder:

About MyNewsletterBuilder API Wrappers:
  http://www.mynewsletterbuilder.com/api/downloads#php
Direct Download of PHP wrapper:
  http://www.mynewsletterbuilder.com/downloads/api/MNB_PHP_API.zip

Unzip and place in sites/all/libraries.